import angular from 'angular';
import notFoundTemplate from './shared/404.html';

function appStates ($stateProvider, $urlRouterProvider, $locationProvider, USER_ROLES) {
  'ngInject';
  // $locationProvider.html5Mode(true);
  
  $urlRouterProvider.otherwise('/404');
  // no route goes to index
  $urlRouterProvider.when('', '/login');
  $urlRouterProvider.when('/', '/login');
  
  $stateProvider
  .state('404', {
    url        : '/404',
    templateUrl: notFoundTemplate,
    data        : { 
      authorizedRoles: [USER_ROLES.all]
    }
  })
  .state('logout', {
    url   : '/logout',
    controller : "logoutController",
    controllerAs: "vm",
    data        : { 
      authorizedRoles: [USER_ROLES.all]
    }
  });
}

function appStateRootInit ($rootScope,$stateParams, $state){
  'ngInject';
  $rootScope.state = $state;
  $rootScope.params = $stateParams;
}

// function appStateRootScope ($rootScope){
//   'ngInject';
//   $rootScope.$on('$stateChangeStart', (event, toState, toParams, fromState, fromParams) => {
//     console.log(toState)
//     console.log(toState.data);
//   });
// }

export default angular.module('app.states',[])
.config(appStates) 
.run(appStateRootInit)
// .run(appStateRootScope)
.name;