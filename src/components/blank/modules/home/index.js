import angular from 'angular';

import subHomeController from './home.controller';
import subHomeService from './home.service';
import subHomeState from './home.state';

export default angular.module('page.user.home',[])
.controller('homeUserController', subHomeController)
.factory('homeUserService', subHomeService)
.config(subHomeState)
.name;