import angular from 'angular';
import moment from 'moment';

function calculateAge () {
  'ngInject';
  return {
    require: "ngModel",
    scope: {
        dateValue: "=calculateAge",
        elementModel: "=ngModel"
    },
    link: function(scope, element, attributes, ngModel) {

      scope.$watch("dateValue", function(oldValue) {
        let current = moment(oldValue);
        let today = moment();
        let dateChecker = moment([today.year(), current.month(), current.date()]);

        if(dateChecker.unix() < today.unix()) {
          scope.elementModel = today.year() - current.year();
        }else {
          scope.elementModel = (today.year() - current.year()) - 1;
        }
      });
    }
  };
}

export default angular.module('custom.calculateAge',[])
.directive('calculateAge', calculateAge)
.name;