//vendor
import angular from 'angular';
import ngMaterial from 'angular-material';
import ngMessages from 'angular-messages';
import ngAnimate from 'angular-animate';
import ngAria from 'angular-aria';
// import ngSpinner from 'angular-spinners';
import ngSanitize from 'angular-sanitize';
// import ngDataTable from 'angular-material-data-table';
// import loadingBar from 'angular-loading-bar';
// import ngTimePicker from 'angular-material-time-picker';
import 'angular-ui-router';
import 'angular-ui-router.statehelper';

//vendor css
// import 'angular-material-time-picker/dist/md-time-picker.css';
// import '../node_modules/angular-loading-bar/src/loading-bar.css';
import '../node_modules/angular-material/angular-material.css';

import './assets/stylesheets/material-custom.css';
import './assets/stylesheets/app.css';
import './assets/stylesheets/main.scss';

import appConfig from './app.config';
import appConstants from './app.constants';

import appStates from './app.states';
import appServices from './app.services';
import appDirectives from './app.directives';
import appProviders from './app.providers';
import appLogger from './app.logger';

import Login from './components/login'
import Main from './components/main'

// import checklistDirective from 'checklist-model';

// import customDate from './shared/custom.date.directive';
// import customEducationalAttainment from './shared/custom.educational.status.directive';
// import customRelationship from './shared/custom.relationship.directive';
// import customEthnicity from './shared/custom.ethnicity.directive';
// import customLanguage from './shared/custom.language.directive';
// import customCompareTo from './shared/custom.compareTo.directive';
// import customCalculateAge from './shared/custom.calculateAge.directive';

var requires = [
    ngMaterial,
    ngMessages,
    ngAnimate,
    ngAria,
    'ui.router',
    'ui.router.stateHelper',
    // loadingBar,
    // ngSpinner,
    ngSanitize,
    appDirectives,
    // ngDataTable,
    
    appConstants,
    appLogger,
    appConfig,
    appStates,
    appServices,
    appProviders,
    // customDate,
    // customEducationalAttainment,
    // customCompareTo,
    // checklistDirective,
    // customRelationship,
    // customEthnicity,
    // customLanguage,
    // customCalculateAge,
    
    Login,
    Main
];

angular.module('prvstrapApp', requires);

if(DEVELOPMENT){
    if(module.hot){
        module.hot.accept();
    }
}