import angular from 'angular';
import moment from 'moment';
import "moment-timezone";
// import 'chart.js';

import wClose from './assets/img/wClose.svg';
import wCancel from './assets/img/wCancel.svg';

import bCancel from './assets/img/bCancel.svg';
import bClose from './assets/img/bClose.svg';

function icons ($mdIconProvider) {
  'ngInject';
  $mdIconProvider
  .icon('navigation:b:close', bClose, 24)
  .icon('dashboard:b:cancel', bCancel, 24)
  
  .icon('navigation:w:close', wClose, 24)
  .icon('dashboard:w:cancel', wCancel, 24)
  ;

}

function colors ($mdThemingProvider) {
  'ngInject'
  $mdThemingProvider.theme('default')
    .primaryPalette('blue')
    .accentPalette('red');
}

function datePickerFormat($mdDateLocaleProvider) {
  'ngInject';
  $mdDateLocaleProvider.formatDate = function(date) {
    console.log(date);
    return moment(date).format('YYYY-MM-DD');
  };
}

export default angular.module('app.icons',[])
.config(colors)
.config(icons) 
// .config(datePickerFormat)
.name;